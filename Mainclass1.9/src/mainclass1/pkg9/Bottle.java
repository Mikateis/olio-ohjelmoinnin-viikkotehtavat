/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainclass1.pkg9;

/*Tekijä: Mika Teiska
 *Päiväys: 29.5.2018
 *IT-Kesäleiri 2018
 */
public class Bottle {
   String name;
   double size;
   double price;
    
    public Bottle(String a, double b, double c){
        name=a;
        size=c;
        price=b;
    }
    
    public String getName(){
        
        return name;
        
    }
    public double getSize(){
        return size;
    }
    public double getPrice(){
        return price;
    }
    
    
}
