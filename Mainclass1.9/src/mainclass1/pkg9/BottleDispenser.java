/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainclass1.pkg9;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/*Tekijä:Mika Teiska
 *Päiväys: 29.5.2018
 * IT-Kesäleiri 2018
 */
public class BottleDispenser {

    private static BottleDispenser instance;
    private int bottles;
    private double money;
    private List<Bottle> pullolista;
    private Bottle last; 

    private BottleDispenser() {
        bottles = 6;
        money = 0;

        pullolista = new ArrayList<Bottle>();

        pullolista.add(new Bottle("Pepsi Max", 1.8, 0.5));
        pullolista.add(new Bottle("Pepsi Max", 2.2, 1.5));
        pullolista.add(new Bottle("Coca-Cola Zero", 2.0, 0.5));
        pullolista.add(new Bottle("Coca-Cola Zero", 2.5, 1.5));
        pullolista.add(new Bottle("Fanta Zero", 1.95, 0.5));
        pullolista.add(new Bottle("Fanta Zero", 1.95, 0.5));
    }

    public static BottleDispenser getInstance() {
        if (instance == null) {
            instance = new BottleDispenser();
        }

        return instance;

    }

    public int getBottles() {
        return bottles;
    }

    public double getMoney() {
        return money;
    }

    public List getPullolista() {
        return pullolista;
    }

    public String addMoney(double maara) {
        money += maara;

        //System.out.printf("%f %f \n", money, maara);
        return "Klink! Lisää rahaa laitteeseen!";
    }

    public String buyBottle(String pullovalinta, String pullovalinta2) {
       
        
        if (pullolista.isEmpty() == true) {
            return " Pullot loppu!";
        }
        for (int i = 0; i < pullolista.size(); i++) {
            
            if (pullolista.get(i).getName().equals(pullovalinta) && Double.toString(pullolista.get(i).getSize()).equals(pullovalinta2)) {
                System.out.println(pullolista.get(i).getName() + pullolista.get(i).getSize());
                if (money < pullolista.get(i).getPrice()) {

                    return "Syötä rahaa ensin!";
                } else {

                    money -= pullolista.get(i).getPrice();
                    last = pullolista.remove(i);
                    return String.format("KACHUNK! %s tipahti masiinasta!",last.getName());
                }
            }
        }
        return "Tuotetta ei ole.";
    }

    public String returnMoney() {
        double tmp = money;
        money = 0;
        return String.format("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f", tmp);

    }

    public void checklista() {
        int i;

        for (i = 0; i < pullolista.size(); i++) {

            System.out.println(i + 1 + ". Nimi: " + pullolista.get(i).getName());
            System.out.println("\tKoko: " + pullolista.get(i).getSize() + "\tHinta: " + pullolista.get(i).getPrice());

        }
    }
public void tulostaKuitti() throws IOException{
   BufferedWriter bw = new BufferedWriter(new FileWriter("kuitti.txt")); 
   bw.write("Kuitti viimeisimmästä pullosta: "+last.getName()+" "+last.getSize()+"L "+last.getPrice()+" euroa.");
   bw.flush();
   
}


}
