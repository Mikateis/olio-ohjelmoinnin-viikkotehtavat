/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainclass1.pkg9;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;

/**
 *
 * @author p8605
 */
public class FXMLDocumentController implements Initializable {

    private Label label;
    @FXML
    private Button returnmoney;
    @FXML
    private Button buybottle;
    @FXML
    private Button addmoney;
    @FXML
    private TextArea textarea;
    @FXML
    private Slider sliderMoney;
    @FXML
    private Label labelMoney;
    @FXML
    private ChoiceBox<String> dropType;
    @FXML
    private ChoiceBox<String> dropSize;
    @FXML
    private Button kuitti;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void returnMoneyButton(ActionEvent event) {
        BottleDispenser bd = BottleDispenser.getInstance();

        textarea.appendText(bd.returnMoney() + "\n");
    }

    @FXML
    private void buyBottleButton(ActionEvent event) {
      
        BottleDispenser bd = BottleDispenser.getInstance();
       String valinta = dropType.getValue();
      String valinta2= dropSize.getValue();
        bd.getPullolista();
        textarea.appendText(bd.buyBottle(valinta,valinta2) + "\n");
        labelMoney.setText(Double.toString(bd.getMoney()));
    }

    @FXML
    private void addMoneyButton(ActionEvent event) {
        BottleDispenser bd = BottleDispenser.getInstance();
        
        textarea.appendText(bd.addMoney(sliderMoney.getValue()) + "\n");
        //String moneystring=Double.toString(sliderMoney.getValue());
        labelMoney.setText(Double.toString(bd.getMoney()));
        //sliderMoney.setValue(0);
    }

    @FXML
    private void kuittiAction(ActionEvent event) throws IOException {
       BottleDispenser bd = BottleDispenser.getInstance();
       bd.tulostaKuitti();
    
    }

}
