/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko11;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;

/*Tekijä:Mika Teiska
 *Päiväys: 14.6.2018
 * IT-Kesäleiri 2018
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Label label;
    @FXML
    private AnchorPane anchor;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    @FXML
    private void putCircleAction(MouseEvent event) {
        ShapeHandler sh = ShapeHandler.getInstance();
        Piste pi = new Piste(anchor);
        pi.setX(event.getX());
        pi.setY(event.getY());
        pi.getCordinates();
        pi.paintCircle();
        anchor.getChildren().add(pi.getCircle());
        sh.addToList(pi.getCircle());
      

    }

}
