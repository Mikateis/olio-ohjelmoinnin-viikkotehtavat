/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko11;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/*Tekijä:Mika Teiska
 *Päiväys: 14.6.2018
 * IT-Kesäleiri 2018
 */
public class Piste {

    private double x;
    private double y;
    int radius = 10;
    Circle piste = new Circle();
   
    private AnchorPane anchor;
    

    public Piste(AnchorPane anchor) {
        this.anchor = anchor;
        final Piste p = this;
        piste.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
           
            @Override
            public void handle(MouseEvent t) {
                ShapeHandler sh = ShapeHandler.getInstance();
                System.out.println("Hei, olen piste!");
                t.consume();
                sh.drawLine(p);

            }
        });
    }

    public void setX(double xarvo) {
        x = xarvo;
        piste.setCenterX(x);
    }

    public void setY(double yarvo) {
        y = yarvo;
        piste.setCenterY(y);
    }

    public void paintCircle() {
        piste.setFill(Color.BLUE);

    }

    public double getX() {

        return x;
    }

    public double getY() {
        return y;
    }
    public AnchorPane getAnchorPane(){
        return anchor;
    }

    public Circle getCircle() {
        return piste;
    }

    public void getCordinates() {

        piste.setRadius(radius);

        System.out.println("X: " + x);
        System.out.println("Y: " + y);

    }

}
