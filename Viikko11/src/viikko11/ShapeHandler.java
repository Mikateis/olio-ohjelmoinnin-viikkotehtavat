/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko11;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 *
 * @author p8605
 */
public class ShapeHandler {

    private static ShapeHandler instance;
    private ArrayList<Circle> pistelista;
    private ArrayList<Line> viivalista;
    private ShapeHandler() {
        pistelista = new ArrayList<Circle>();
        viivalista = new  ArrayList<Line>();
    }

    public static ShapeHandler getInstance() {
        if (instance == null) {
            instance = new ShapeHandler();
        }

        return instance;

    }

    public void addToList(Circle s) {
        pistelista.add(s);

    }
    private double x;
    private double y;
    private boolean parillisuus = false;
    private Line line;

    public void drawLine(Piste pi) {

        if (parillisuus == false) {
            x = pi.getX();
            y = pi.getY();
            System.out.println("hei nyt tulosta jo");

        } else {
           // if (line == null) {
                line = new Line();
             pi.getAnchorPane().getChildren().add(line);
           // }

            line.setStartX(x);
            line.setStartY(y);
            line.setEndX(pi.getX());
            line.setEndY(pi.getY());
            viivalista.add(line);
            
           
           

        }
        parillisuus = !parillisuus;

    }

    public List getList() {
        return pistelista;
    }
    
   

}
