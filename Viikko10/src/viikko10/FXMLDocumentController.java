/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko10;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;

/*Tekijä:Mika Teiska
 *Päiväys: 13.6.2018
 * IT-Kesäleiri 2018
 */
public class FXMLDocumentController implements Initializable {

    private Label label;
    @FXML
    private TextField osoitepalkki;
    @FXML
    private WebView web;
    @FXML
    private Button RefreshButton;
    @FXML
    private Button rajapintaButton;
    @FXML
    private Insets x1;
    @FXML
    private Button ScriptButton;
    @FXML
    private Button init;

    ArrayList<String> sivuhistoria = new ArrayList();
    @FXML
    private Button leftArrow;
    @FXML
    private Button rightArrow;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        web.getEngine().load("http://www.lut.fi");
        sivuhistoria.add("http://www.lut.fi");
    }
    static int number = 0;

    @FXML
    private void onSearchAction(ActionEvent event) {
        String osoite = "http://" + osoitepalkki.getText();
        if(number<=10){
        if (number!=sivuhistoria.size()){
           for (int i=sivuhistoria.size()-1;i>number;i--){
               sivuhistoria.remove(i);
           }
            
        }
        sivuhistoria.add(osoite);
        number++;
        }else{
            
            sivuhistoria.remove(0);
            sivuhistoria.add(osoite);
            
        }

        web.getEngine().load(osoite);

        if ("index.html".equals(osoite)) {

            web.getEngine().load(getClass().getResource("index.html").toExternalForm());
        }

    }

    @FXML
    private void onSiteRefreshAction(ActionEvent event) {
        web.getEngine().reload();
        osoitepalkki.setText(web.getEngine().getLocation());

    }

    @FXML
    private void loadLocalFile(ActionEvent event) {
        web.getEngine().load(getClass().getResource("index.html").toExternalForm());

    }

    @FXML
    private void doScript(ActionEvent event) {
        web.getEngine().executeScript("document.shoutOut()");
    }

    @FXML
    private void InitializeAction(ActionEvent event) {
        web.getEngine().executeScript("initialize()");
    }

    @FXML

    private void leftArrowAction(ActionEvent event) {

        if (number > 0) {
           
            number--;
            
            web.getEngine().load(sivuhistoria.get(number));

            osoitepalkki.setText(web.getEngine().getLocation());

        }
    }

    @FXML
    private void rightArrowAction(ActionEvent event) {
                if (number < sivuhistoria.size()-1) {

            web.getEngine().load(sivuhistoria.get(number + 1));

            osoitepalkki.setText(web.getEngine().getLocation());
            number++;

        }
    }

}
