/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht;

/*Tekijä:Mika Teiska
 *Päiväys: 5.7.2018
 * IT-Kesäleiri 2018
 */
public class Automaatti {

    private String automaatinnimi;
    private String city;
    private String postalcode;
    private String address;
    private String ajat;
    private String lat;
    private String lng;

    public Automaatti(String nimi, String kaupunki, String postinumero, String osoite, String aukioloajat, String lat, String lng) {
        automaatinnimi = nimi;
        city = kaupunki;
        postalcode = postinumero;
        address = osoite;
        ajat = aukioloajat;
        this.lat = lat;
        this.lng = lng;

    }

    public String getAutomaatinnimi() {
        return automaatinnimi;
    }

    public String getCity() {
        return city;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public String getAjat() {
        return ajat;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return automaatinnimi;
    }

}
