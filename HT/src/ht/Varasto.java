/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mika1
 */
public class Varasto {

    private static Varasto va = null;
    private int id;
    private List<Pakettiluokka> paketti;

    private Varasto(int id) {
        this.id = id;
        paketti = new ArrayList<>();

    }

    public static Varasto getInstance() {

        if (va == null) {
            va = new Varasto(1);
        }

        return va;
    }

    public void addLista(Pakettiluokka luokka) {
        paketti.add(luokka);

    }
}
