

CREATE TABLE esine(
	"esineID"	INTEGER PRIMARY KEY,
	"hajoavuus"	BOOLEAN,
	"nimi"  VARCHAR(64),
	"massa" INTEGER,
	"tilavuus" INTEGER,
	CHECK (massa>0)
	);

CREATE TABLE varasto(
	"varastoID"	PRIMARY KEY

);
--Paketilla ja esineellä voi periaatteessa olla eri massat sekä tilavuudet, mutta tämänhetkinen toteutus asettaa samat arvot
CREATE TABLE paketti (
	"pakettiID"  INTEGER PRIMARY KEY,
	"luokka" INTEGER,
	"massa" INTEGER,
	"tilavuus" INTEGER, 
	"lahetetty" BOOLEAN,
	CHECK(tilavuus>0),
	CHECK(massa>0),
	CHECK(luokka>0)

	
);

CREATE TABLE paikkakuntatiedot(
"postinumero" VARCHAR(32) PRIMARY KEY,
"kaupunki" VARCHAR(32)
);


CREATE TABLE smartpost(
	"automaattiID" INTEGER PRIMARY KEY,
	"lat"	VARCHAR(32)  NOT NULL,
	"lng"	VARCHAR(32) NOT NULL ,	
	"aukioloaika"	VARCHAR(64) NOT NULL,
	"nimi" 	VARCHAR(32) NOT NULL,
	"osoite" VARCHAR(32) NOT NULL,
	"varastoID"     INTEGER NOT NULL,
	"postinumero" VARCHAR(32), 
	CHECK(automaattiID>1 OR automaattiID=NULL),

	FOREIGN KEY ("varastoID") REFERENCES varasto ("varastoID"),
	FOREIGN KEY ("postinumero") REFERENCES paikkakuntatiedot ("postinumero") ON UPDATE CASCADE ON DELETE CASCADE

	
);


CREATE TABLE paketinlahetys(
	"automaattiID" INTEGER PRIMARY KEY,
	"pakettiID"	INTEGER,	

	FOREIGN KEY ("pakettiID") REFERENCES "paketti" ("pakettiID") ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY ("automaattiID") REFERENCES "smartpost" ("automaattiID") ON UPDATE CASCADE ON DELETE CASCADE
);


CREATE TABLE pakkaus(
"pakettiID" INTEGER PRIMARY KEY,
"esineID" INTEGER, 
FOREIGN KEY ("pakettiID") REFERENCES "paketti" ("pakettiID") ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY ("esineID") REFERENCES "esine" ("esineID")  ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE varastossa(
"pakettiID" INTEGER PRIMARY KEY,
"varastoID" INTEGER,
FOREIGN KEY ("pakettiID") REFERENCES "paketti" ("pakettiID") ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY ("varastoID") REFERENCES "varasto" ("varastoID") ON UPDATE CASCADE ON DELETE CASCADE
);

/*INSERT INTO "esine"
	VALUES (NULL,'TRUE', 'Levy',20,20);

INSERT INTO "esine"
	VALUES (NULL,'FALSE', 'Yksisarvinen',500,500);


INSERT INTO "varasto"
	VALUES (NULL);

INSERT INTO "paketti"
	VALUES (NULL,1,40,20);

INSERT INTO "paketti"
	VALUES (NULL,3,20,30);
*/

