/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht;

/**
 *
 * @author p8605
 */
public class Esine {

    private String name;
    private int tilavuus;
    private int massa;
    private boolean hajoavuus;
    private int EsineID;

    public Esine(String nimi, int tilavuus, int massa, boolean hajoavuus, int esineID) {
        name = nimi;
        this.tilavuus = tilavuus;
        this.massa = massa;
        this.hajoavuus = hajoavuus;
        this.EsineID = esineID;

    }

    public String getName() {
        return name;
    }

    public int getTilavuus() {
        return tilavuus;
    }

    public int getMassa() {
        return massa;
    }

    public boolean isHajoavuus() {
        return hajoavuus;
    }

    @Override
    public String toString() {
        return name;
    }

    public int getEsineID() {
        return EsineID;
    }

}
