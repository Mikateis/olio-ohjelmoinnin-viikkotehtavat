/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author p8605
 */
public class Tietokantakasittely {

    public ArrayList<String> automaattiIDlista;

    public void Tietokantakasittely() {

    }

    public void tietokantakysely(ArrayList<Automaatti> automaattilista) throws SQLException {

        Connection con = DriverManager.getConnection("jdbc:sqlite:HT.db");
        Statement state = con.createStatement();
        try {
            con.setAutoCommit(false);
        } catch (SQLException ex) {
            Logger.getLogger(Tietokantakasittely.class.getName()).log(Level.SEVERE, null, ex);
        }

        ResultSet rs = state.executeQuery("SELECT COUNT (*) FROM smartpost");
        rs.next();

        if (rs.getInt(1) == 0) {
            for (Automaatti a : automaattilista) {

                try {

                    state.executeUpdate("INSERT INTO paikkakuntatiedot VALUES ('" + a.getPostalcode() + "','" + a.getCity() + "')");
                } catch (SQLException sQLException) {
                }

                /*katso myöhemmin paikkatiedot - automaatti yhteys*/
                state.executeUpdate("INSERT INTO smartpost VALUES (NULL,'"
                        + a.getLat() + "','" + a.getLng() + "','" + a.getAjat() + "','"
                        + a.getAutomaatinnimi() + "','" + a.getAddress() + "'," + '1' + ",'" + a.getPostalcode() + "')"
                );

            }
        }

        con.commit();

        con.setAutoCommit(true);
        con.close();

    }

    public ArrayList<String> getKaupunki() {
        ArrayList<String> kaupunkilista;
        kaupunkilista = new ArrayList();
        Connection con;
        try {
            con = DriverManager.getConnection("jdbc:sqlite:HT.db");
            Statement state = con.createStatement();
            ResultSet rs1 = state.executeQuery("SELECT DISTINCT kaupunki FROM paikkakuntatiedot ORDER BY kaupunki");

            while (rs1.next()) {

                kaupunkilista.add(rs1.getString("kaupunki"));

            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Tietokantakasittely.class.getName()).log(Level.SEVERE, null, ex);
        }

        return kaupunkilista;
    }

    public ArrayList<Automaatti> getAutomaatitFromkaupunki(String kaupunki) throws SQLException {
        ArrayList<Automaatti> automaatitkaupungeista = new ArrayList();
        Connection con;
        con = DriverManager.getConnection("jdbc:sqlite:HT.db");
        Statement state = con.createStatement(); //Sovelletaan INNER JOINIA tauluja yhdistelemiseen.
        ResultSet rs2 = state.executeQuery("SELECT * FROM smartpost "
                + " INNER JOIN paikkakuntatiedot ON smartpost.postinumero=paikkakuntatiedot.postinumero WHERE kaupunki ='" + kaupunki + "'");
        while (rs2.next()) {

            String lat = rs2.getString("lat");
            String lng = rs2.getString("lng");
            String aukioloaika = rs2.getString("aukioloaika");
            String nimi = rs2.getString("nimi");
            String osoite = rs2.getString("osoite");

            String postinumero = rs2.getString("postinumero");
            Automaatti a = new Automaatti(nimi, kaupunki, postinumero, osoite, aukioloaika, lat, lng);
            automaatitkaupungeista.add(a);
        }
        con.close();
        return automaatitkaupungeista;
    }

    public void addItems(Esine e) throws SQLException {
        Connection con;
        con = DriverManager.getConnection("jdbc:sqlite:HT.db");
        Statement state = con.createStatement();

        state.executeUpdate("INSERT INTO esine VALUES(NULL,'" + e.isHajoavuus() + "','"
                + e.getName() + "','" + e.getMassa() + "','" + e.getTilavuus() + "')");
        con.close();
    }

    public ArrayList<Esine> getEsineet() throws SQLException {
        ArrayList<Esine> tietokannanesinelista = new ArrayList();
        Connection con;
        con = DriverManager.getConnection("jdbc:sqlite:HT.db");
        Statement state = con.createStatement();

        ResultSet rs2 = state.executeQuery("SELECT * FROM esine;");
        while (rs2.next()) {
            int EsineID = rs2.getInt("esineID");
            Boolean hajoavuus = rs2.getBoolean("hajoavuus");
            String nimi = rs2.getString("nimi");
            int massa = rs2.getInt("massa");
            int tilavuus = rs2.getInt("tilavuus");

            Esine e = new Esine(nimi, tilavuus, massa, hajoavuus, EsineID);
            tietokannanesinelista.add(e);

        }
        con.close();
        return tietokannanesinelista;
    }

    public void addPackage(Pakettiluokka luokka) throws SQLException {

        Connection con;
        con = DriverManager.getConnection("jdbc:sqlite:HT.db");
        Statement state = con.createStatement();

        state.executeUpdate("INSERT INTO paketti VALUES (NULL,'" + luokka.getLuokka() + "','" + luokka.getEsine().getMassa() + "','"
                + luokka.getEsine().getTilavuus() + "','" + luokka.isLahetetty() + "')");
        state.executeUpdate("INSERT INTO varastossa VALUES (NULL,1)");
        state.executeUpdate("INSERT INTO pakkaus VALUES (NULL,'" + luokka.getEsine().getEsineID() + "')");
        Varasto.getInstance().addLista(luokka);

    }

    public void addVarasto() {
        try {
            Connection con;
            con = DriverManager.getConnection("jdbc:sqlite:HT.db");
            Statement state = con.createStatement();
            state.executeUpdate("INSERT INTO varasto VALUES (1)");
            Varasto.getInstance();
            con.close();
        } catch (SQLException ex) {

        }

    }

    public void updatePackage(Pakettiluokka paketti) throws SQLException {
        Connection con;
        con = DriverManager.getConnection("jdbc:sqlite:HT.db");
        Statement state = con.createStatement();
        state.executeUpdate("UPDATE paketti SET lahetetty = 'true' WHERE pakettiID = (SELECT pakettiID FROM paketti ORDER BY pakettiID DESC LIMIT 1)");

        con.close();
    }

    public void addPakkauksenlahetys(String nimi) throws SQLException { //addPakkauksenlahetys laittaa lähetetyn paketin automaattiID:n tauluun paketinlahetys

        Connection con;
        con = DriverManager.getConnection("jdbc:sqlite:HT.db");
        Statement state = con.createStatement();
        ResultSet rs3 = state.executeQuery("SELECT automaattiID FROM smartpost WHERE nimi ='" + nimi + "'");
        while (rs3.next()) {
            String automaattiid = rs3.getString("automaattiID");
            state.executeUpdate("INSERT INTO paketinlahetys VALUES (NULL,'" + automaattiid + "')");

        }
        con.close();
    }
}
