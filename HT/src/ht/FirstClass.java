/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht;

/**
 *
 * @author p8605
 */
public class FirstClass extends Pakettiluokka {

    static double maxetaisyys = 150.0;
    int luokka = 1;
    boolean hajoavuus = true;

    public FirstClass(int luokka, Esine esine, boolean lahetetty) {
        super(luokka, esine, lahetetty);
    }

    public static double getmaxetaisyys() {
        return maxetaisyys;
    }

    public Esine getEsine() {
        return esine;
    }

}
