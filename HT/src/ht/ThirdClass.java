/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht;

/**
 *
 * @author p8605
 */
public class ThirdClass extends Pakettiluokka {

    int maxkoko = 80;
    int maxmassa = 80;
    int luokka = 3;
    boolean hajoavuus = true;

    public ThirdClass(int luokka, Esine esine, boolean lahetetty) {
        super(luokka, esine, lahetetty);
    }

    public int getMaxkoko() {
        return maxkoko;
    }

    public int getMaxmassa() {
        return maxmassa;
    }

    public int getLuokka() {
        return luokka;
    }

}
