/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/*Tekijä:Mika Teiska
 *Päiväys: 5.7.2018
 * IT-Kesäleiri 2018
 */
public class XMLpurku {

    private Document doc;
    private ArrayList<Automaatti> automaattilista;

    public ArrayList XMLpurku() throws IOException, SQLException {

        automaattilista = new ArrayList<>();
        DocumentBuilder dBuilder = null;

        String s, total = "";
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try {
            dBuilder = dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XMLpurku.class.getName()).log(Level.SEVERE, null, ex);
        }
        URL url = null;
        try {
            url = new URL("http://iseteenindus.smartpost.ee/api/?request=destinations&country=FI&type=APT");
        } catch (MalformedURLException ex) {
            Logger.getLogger(XMLpurku.class.getName()).log(Level.SEVERE, null, ex);
        }
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(url.openStream(), "utf-8"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(XMLpurku.class.getName()).log(Level.SEVERE, null, ex);
        }

        while ((s = br.readLine()) != null) {
            total += s + "\n";
        }
        try {
            doc = (Document) dBuilder.parse(new InputSource(new StringReader(total)));
        } catch (SAXException ex) {
            Logger.getLogger(XMLpurku.class.getName()).log(Level.SEVERE, null, ex);
        }
        doc.getDocumentElement().normalize();
        NodeList nodes = doc.getElementsByTagName("item");

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            String nimi = e.getElementsByTagName("name").item(0).getTextContent();
            String kaupunki = e.getElementsByTagName("city").item(0).getTextContent();
            String postinumero = e.getElementsByTagName("postalcode").item(0).getTextContent();
            String osoite = e.getElementsByTagName("address").item(0).getTextContent();
            String aukioloajat = e.getElementsByTagName("availability").item(0).getTextContent();
            String lat = e.getElementsByTagName("lat").item(0).getTextContent();
            String lng = e.getElementsByTagName("lng").item(0).getTextContent();
            Automaatti au = new Automaatti(nimi, kaupunki, postinumero, osoite, aukioloajat, lat, lng);
            automaattilista.add(au);

        }
        System.out.println("XML purettu.");
        return automaattilista;
    }
}
