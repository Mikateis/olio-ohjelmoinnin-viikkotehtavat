/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import static javafx.scene.paint.Color.web;

import javafx.scene.web.WebView;

/*Tekijä:Mika Teiska
 *Päiväys: 5.7.2018
 * IT-Kesäleiri 2018
 */
public class FXMLDocumentController implements Initializable {

    ArrayList<Automaatti> automaattikaupunkilista;
    ArrayList<Esine> esinelista; //Esinelista pitää kirjaa luoduista esineistä sekä tietokannasta luetuista esineistä
    ArrayList<String> koordinaattilista;
    Tietokantakasittely ti = new Tietokantakasittely();
    @FXML
    private Button AutomaattiMapButton;
    @FXML
    private WebView kartta;
    @FXML
    private ComboBox<String> KaupunkiCombo;
    @FXML
    private ComboBox<Esine> ValitseEsineetComboBox;
    @FXML
    private TextField nimiField;
    @FXML
    private CheckBox HajoavaaBox;
    @FXML
    private TextField MassaField;
    @FXML
    private TextField TilavuusField;
    @FXML
    private ComboBox<String> lahtoKaupunki;
    @FXML
    private ComboBox<String> kohdeKaupunki;
    @FXML
    private ComboBox<Automaatti> lahtoAutomaatti;
    @FXML
    private ComboBox<Automaatti> kohdeAutomaatti;
    @FXML
    private ComboBox<String> PakettiluokkaBox;
    @FXML
    private Label labelPaketinlahetys;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        try {
            //Valmistellaaan kaikki tarvittavat kentät initializessa
            XMLpurku xml = new XMLpurku();
            ti.tietokantakysely(xml.XMLpurku());

            automaattikaupunkilista = new ArrayList();
            koordinaattilista = new ArrayList();
            esinelista = new ArrayList();
            esinelista = ti.getEsineet();

            for (int i = 0; i < esinelista.size(); i++) {
                ValitseEsineetComboBox.getItems().addAll(esinelista.get(i));

            }

            ArrayList<String> luokkalista = new ArrayList();
            luokkalista.add("1");    //Lisätään eri pakettiluokat listaan josta saadaan pakettiluokat luettua Comboboxiin
            luokkalista.add("2");
            luokkalista.add("3");

            KaupunkiCombo.getItems().addAll(ti.getKaupunki());
            lahtoKaupunki.getItems().addAll(ti.getKaupunki());
            kohdeKaupunki.getItems().addAll(ti.getKaupunki());

            PakettiluokkaBox.getItems().addAll(luokkalista);
            ti.addVarasto();
            kartta.getEngine().load(this.getClass().getResource("index(1).html").toString());
            Class.forName("org.sqlite.JDBC");

        } catch (IOException | SQLException | ClassNotFoundException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
//Näytetään Automaatit kartalla soveltamalla Javascriptiä

    @FXML
    private void showAutomaattiMapAction(ActionEvent event) throws SQLException {

        automaattikaupunkilista = ti.getAutomaatitFromkaupunki(KaupunkiCombo.getValue());
        for (Automaatti c : automaattikaupunkilista) {
            kartta.getEngine().executeScript("document.goToLocation('" + c.getAddress() + " " + c.getPostalcode() + " "
                    + c.getCity() + "','" + c.getAutomaatinnimi() + " " + c.getAjat() + "','purple')");
        }
    }

    @FXML
    private void luoEsineAction(ActionEvent event) {

        //Esinelistaan lisätään esine Olioit, josta ne voidaan lisätä ComboBoxiin
        int e = esinelista.size();
        Esine es = new Esine(nimiField.getText(), Integer.parseInt(TilavuusField.getText()), Integer.parseInt(MassaField.getText()),
                HajoavaaBox.isSelected(), e + 1);
        esinelista.add(es);
        ValitseEsineetComboBox.getItems().add(es);
        try {
            ti.addItems(es);
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    @FXML
    private void luoPakettiAction(ActionEvent event) throws SQLException {//Luodaan paketti poimimalla haluttu esine ja luokka ja luodaan tämän mukainen pakettiluokka.
        try {
            Pakettiluokka paketti = null;
            Esine esine = ValitseEsineetComboBox.getSelectionModel().getSelectedItem();
            String lahtopaikka = lahtoKaupunki.getSelectionModel().getSelectedItem();
            String kohdepaikka = kohdeKaupunki.getSelectionModel().getSelectedItem();
            int luokka = Integer.parseInt(PakettiluokkaBox.getValue());
            switch (luokka) { // katsotaan minkä luokan käyttäjä valitsee ja luodaan tämän mukainen pakettiolio
                case 1:
                    paketti = new FirstClass(luokka, esine, false);//Pakettiluokkaa extendaava olio saa parametreikseen luokan ja esine Olion.
                    ti.addPackage(paketti);
                    break;
                case 2:
                    paketti = new SecondClass(luokka, esine, false);
                    ti.addPackage(paketti);
                    break;
                default:
                    paketti = new ThirdClass(luokka, esine, false);
                    ti.addPackage(paketti);
                    break;
            }
            sendPaketti(paketti);
        } catch (NullPointerException ex) {
            labelPaketinlahetys.setText("Varmista että olet syöttänyt tarvittavat tiedot.");

        }

    }

    @FXML
    private void fillStartAutomaattiaction(ActionEvent event) {//Lisätään lähtöautomaatit-comboboxiin tietyn kaupungin automaatit

        lahtoAutomaatti.getItems().clear();
        try {
            automaattikaupunkilista = ti.getAutomaatitFromkaupunki(lahtoKaupunki.getValue());
            for (int i = 0; i < automaattikaupunkilista.size(); i++) {
                lahtoAutomaatti.getItems().add(automaattikaupunkilista.get(i));

            }
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void fillEndAutomaattiAction(ActionEvent event) { //Lisätään kohdeautomaatit-comboboxiin tietyn kaupungin automaatit
        kohdeAutomaatti.getItems().clear();
        try {
            automaattikaupunkilista = ti.getAutomaatitFromkaupunki(kohdeKaupunki.getValue());
            for (int i = 0; i < automaattikaupunkilista.size(); i++) {
                kohdeAutomaatti.getItems().add(automaattikaupunkilista.get(i));
            }
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void sendPaketti(Pakettiluokka paketti) throws SQLException { //Sendpaketti suorittaa document.createPath scriptin ja näin lähettää paketin
        labelPaketinlahetys.setText("");
        int luokka = Integer.parseInt(PakettiluokkaBox.getValue());
        Esine esine = ValitseEsineetComboBox.getSelectionModel().getSelectedItem();

        String Startlat = lahtoAutomaatti.getSelectionModel().getSelectedItem().getLat();
        String Startlng = lahtoAutomaatti.getSelectionModel().getSelectedItem().getLng();
        String Endlat = kohdeAutomaatti.getSelectionModel().getSelectedItem().getLat();
        String Endlng = kohdeAutomaatti.getSelectionModel().getSelectedItem().getLng();
        String automaatinnimi = lahtoAutomaatti.getSelectionModel().getSelectedItem().getAutomaatinnimi();
        koordinaattilista.add(Startlat);
        koordinaattilista.add(Startlng);
        koordinaattilista.add(Endlat);
        koordinaattilista.add(Endlng);

        double etaisyys = Double.parseDouble(kartta.getEngine().executeScript("document.checkdistance(" + koordinaattilista + ")").toString()); //Luodaan Javascript metodi joka palauttaa vainv automaattien välisen etäisyyden
        if (luokka == 1) {
            if (etaisyys < FirstClass.getmaxetaisyys()) {
                kartta.getEngine().executeScript("document.createPath(" + koordinaattilista + ",'purple'," + 1 + ")");
                paketti.setLahetetty(true);
                ti.updatePackage(paketti);
                if (paketti.getEsine().isHajoavuus() == true) {
                    labelPaketinlahetys.setText("Hups esine meni rikki.");

                }
            } else {
                labelPaketinlahetys.setText("Pakettiluokan maksimi lähetyspituus ylittyi.");

            }
        } else if (luokka == 2) {

            kartta.getEngine().executeScript("document.createPath(" + koordinaattilista + ",'purple'," + 2 + ")");
            paketti.setLahetetty(true);
            ti.updatePackage(paketti);
            if (paketti.getEsine().isHajoavuus() == true) {
                if (paketti.getEsine().getTilavuus() > SecondClass.getmaxtilavuus()) {
                    labelPaketinlahetys.setText("Hups esine meni rikki,sillä se oli liian suuri.");
                }
            }
        } else {
            kartta.getEngine().executeScript("document.createPath(" + koordinaattilista + ",'purple'," + 3 + ")");
            paketti.setLahetetty(true);
            ti.updatePackage(paketti);
            if (paketti.getEsine().isHajoavuus() == true) {
                labelPaketinlahetys.setText("Hups esine meni rikki.");

            }
        }
        koordinaattilista.clear();
        ti.addPakkauksenlahetys(automaatinnimi);
    }

    @FXML
    public void deletePaths() {
        kartta.getEngine().executeScript("document.deletePaths()");
    }

}
