/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht;

import java.util.ArrayList;

/*Tekijä:Mika Teiska
 *Päiväys: 18.7.2018
 * IT-Kesäleiri 2018
 */
public class Pakettiluokka {

    private int luokka;
    protected Esine esine;
    boolean lahetetty;

    public Pakettiluokka(int luokka, Esine esine, boolean lahetetty) {

        this.luokka = luokka;
        this.esine = esine;
        this.lahetetty = lahetetty;

    }

    public int getLuokka() {
        return luokka;
    }

    public Esine getEsine() {
        return esine;
    }

    public void setLahetetty(boolean lahetetty) {
        this.lahetetty = lahetetty;
    }

    public boolean isLahetetty() {
        return lahetetty;
    }

}
