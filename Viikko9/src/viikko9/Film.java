/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko9;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author p8605
 */
public class Film {

    Date alkamisaika;
    String Title;
    Date loppumisaika;
    String Teatteri;

    public Film(String a, Date b, Date c, String d) {
        alkamisaika = b;
        loppumisaika = c;
        Title = a;
        Teatteri = d;
    }

    @Override
    public String toString() {

        
       
        SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        String s = format.format(alkamisaika);
       
        return Title + " " + Teatteri + " " + s;

    }

}
