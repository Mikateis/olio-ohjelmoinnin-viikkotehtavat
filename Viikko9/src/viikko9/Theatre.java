/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko9;

/*Tekijä:Mika Teiska
 *Päiväys: 7.6.2018
 * IT-Kesäleiri 2018
 */
public class Theatre {

    String paikka;
    String id;

    public Theatre(String a, String b) {
        id = a;
        paikka = b;
    }

    public String getId() {
        return id;
    }
    @Override
    public String toString() {
        return paikka;
    }

    public String getName() {
        return paikka; 
    }

}
