/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko9;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import org.xml.sax.SAXException;

/*Tekijä:Mika Teiska
 *Päiväys: 7.6.2018
 * IT-Kesäleiri 2018
 */
public class FXMLDocumentController implements Initializable {

    private Label label;
    @FXML
    private ChoiceBox<Theatre> teatteriChoice;
    @FXML
    private TextField esityspva;
    @FXML
    private TextField alkamisaika;
    @FXML
    private TextField paattymisaika;
    @FXML
    private Button ListaaButton;
    @FXML
    private TextField elokuvanimi;
    @FXML
    private Button nimiHakuButton;
    @FXML
    private ListView<Film> ListView;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            Tietorakenne ti = new Tietorakenne();

            ti.readURL("https://www.finnkino.fi/xml/TheatreAreas/");
            ti.getCurrentData();
            teatteriChoice.getItems().clear();
            teatteriChoice.getItems().addAll(ti.getList());
            teatteriChoice.getSelectionModel().selectFirst();

        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void ListMoviesAction(ActionEvent event) throws MalformedURLException, IOException, SAXException, ParseException {
        elokuvanimi.setText("");
        getTiedot();
    }

    @FXML
    private void nimiHakuAction(ActionEvent event) throws IOException, MalformedURLException, SAXException, ParseException {

        getTiedot();

    }

    public void getTiedot() throws ParseException, IOException, MalformedURLException, SAXException {
        String Datetime = new SimpleDateFormat("dd.MM.yyyy").format(Calendar.getInstance().getTime());
        Tietorakenne ti = new Tietorakenne();
        if (esityspva.getText().isEmpty()) {

        } else {
            Datetime = esityspva.getText();

        }
        ti.readTheatre(teatteriChoice.getValue().getId(), Datetime);

        ti.getTheatreData(teatteriChoice.getValue().getName());
        ListView.getItems().clear();

        DateFormat format = new SimpleDateFormat("HH:mm", Locale.ENGLISH);

        
        if (alkamisaika.getText().isEmpty()){
            alkamisaika.setText("1:00");
        }
        if (paattymisaika.getText().isEmpty()){
            paattymisaika.setText("24:00");
        }
                
        
        Date date = format.parse(alkamisaika.getText());
        Date date2 = format.parse(paattymisaika.getText());
        String elokuva = (elokuvanimi.getText());

        ListView.getItems().addAll(ti.filterList(date, date2, elokuva));

    }
}
