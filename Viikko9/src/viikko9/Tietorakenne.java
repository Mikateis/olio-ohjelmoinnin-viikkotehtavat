/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/*Tekijä:Mika Teiska
 *Päiväys: 7.6.2018
 * IT-Kesäleiri 2018
 */
public class Tietorakenne {

    private ArrayList<Theatre> tietolista;
    private ArrayList<Film> filmilista;
    ArrayList<Film> uusifilmilista = new ArrayList();
    private Document doc;
    // String paikka;
    // String id;

    public Tietorakenne() {
        tietolista = new ArrayList<>();
        filmilista = new ArrayList<>();
    }

    public void readURL(String a) throws MalformedURLException, IOException, SAXException {
        DocumentBuilder dBuilder = null;
        try {

            String s, total = "";
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            dBuilder = dbFactory.newDocumentBuilder();
            URL url = new URL(a);
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream(), "utf-8"));

            while ((s = br.readLine()) != null) {
                total += s + "\n";
            }
            doc = (Document) dBuilder.parse(new InputSource(new StringReader(total)));
            doc.getDocumentElement().normalize();

        } catch (ParserConfigurationException ex) {
            Logger.getLogger(Theatre.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            System.out.println("Operation is done.");
        }

    }

    public void readTheatre(String id, String date) throws IOException, MalformedURLException, SAXException {

        readURL(String.format("http://www.finnkino.fi/xml/Schedule/?area=%s&dt=%s", id, date));

    }

    protected void getCurrentData() {

        NodeList nodes = doc.getElementsByTagName("TheatreArea");
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            String ID = e.getElementsByTagName("ID").item(0).getTextContent();
            String Paikka = e.getElementsByTagName("Name").item(0).getTextContent();
            Theatre th = new Theatre(ID, Paikka);
            tietolista.add(th);

        }

    }

    protected void getTheatreData(String teatteri) throws ParseException {
        getTheatreData("", teatteri);

    }

    protected void getTheatreData(String s, String teatteri) throws ParseException {
        NodeList nodes = doc.getElementsByTagName("Show");
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;

            String Title = e.getElementsByTagName("Title").item(0).getTextContent();

            if (!Title.trim().toLowerCase().contains(s.trim().toLowerCase())) {
                continue;
            }
            String alkamisAika = e.getElementsByTagName("dttmShowStart").item(0).getTextContent();
            String loppumisAika = e.getElementsByTagName("dttmShowEnd").item(0).getTextContent();
            alkamisAika = alkamisAika.substring(alkamisAika.indexOf("T") + 1, alkamisAika.length() - 3);
            loppumisAika = loppumisAika.substring(loppumisAika.indexOf("T") + 1, loppumisAika.length() - 3);
            DateFormat format = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
            Date date = format.parse(alkamisAika);
            Date date2 = format.parse(loppumisAika);

            Film film = new Film(Title, date, date2, teatteri);

            filmilista.add(film);

        }
    }

    public ArrayList getList() {
        return tietolista;

    }

    protected ArrayList<Film> filterList(Date kayttajanalkamisaika, Date kayttajanpaattymisaika, String elokuvannimi) {
        ArrayList<Film> uusi = new ArrayList<>();
        uusi.addAll(filmilista);
        for (Film f : filmilista) {
            if (f.alkamisaika.before(kayttajanalkamisaika) || f.alkamisaika.after(kayttajanpaattymisaika)) {
                uusi.remove(f);
                continue;
            }
            if (!elokuvannimi.isEmpty()) {

                if (!f.Title.contains(elokuvannimi)) {
                    uusi.remove(f);
                }
            }

        }
        return uusi;

    }

    public ArrayList getFilmlist() {
        return filmilista;
    }

}
